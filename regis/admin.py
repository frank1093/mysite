from django.contrib import admin
from django import forms
from .models import Register
# Register your models here.


class RegisterAdmin(admin.ModelAdmin):
	search_fields = ['identity_card', 'MAC_address', 'firstname', 'lastname']
	list_display = ['identity_card', 'firstname', 'lastname', 'MAC_address', 'timestamp', 'modified', 'confirmed_by']
	class Meta:
		model = Register

admin.site.register(Register, RegisterAdmin)