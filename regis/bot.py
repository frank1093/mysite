import mechanize


def enroll(data):
    browser = mechanize.Browser()
    browser.set_handle_robots(False)
    browser.open("https://smart.ku.ac.th/")
    browser.select_form(name="loginform")

    browser["username"] = data[0]
    browser["password"] = data[1]
    control = browser.form.find_control("zoneid")
    control.value = ['-2']
    response = browser.submit()

    response = browser.open("https://smart.ku.ac.th/index.php?content=regist&topic=usermac&mactype=wireless&action=addmac&lang=th")
    # print response.read()
    for i in response:
        if (i.count("Personal Computer addition") >= 1 ):
            return "Login Fail"
    browser.select_form(name="addmac")
    control = browser.form.find_control("orgid")
    control.value = ['8']
    control = browser.form.find_control("osid")
    control.value = [changeOSToValue(data[2])]
    browser["mac"] = data[3]
    browser["building"] = "IUP"

    # control = browser.form.find_control("deviceid")
    # control.value = [changeTypeToValue(data[4])]
    browser["telephone"] = data[5]
    browser["email"] = data[6]
    print data[0], data[1], changeOSToValue(data[2]), data[3], changeTypeToValue(data[4]), data[5], data[6]
    response = browser.submit()

    response = browser.open("https://smart.ku.ac.th/index.php?content=regist&topic=usermac&lang=th")
    for i in response:
        if (i.count(data[3]) >= 1 ):
            return "Success"
    return "Login Fail"



def countDevice(data):
    browser = mechanize.Browser()
    browser.set_handle_robots(False)
    browser.open("https://smart.ku.ac.th/")
    browser.select_form(name="loginform")

    browser["username"] = data[0]
    browser["password"] = data[1]
    control = browser.form.find_control("zoneid")
    control.value = ['-2']
    response = browser.submit()

    response = browser.open("https://smart.ku.ac.th/index.php?content=regist&topic=usermac&lang=th")
    for i in response:
        if (i.count("Personal Computer addition") >= 1 ):
            return "Login Fail"
    response = browser.open("https://smart.ku.ac.th/index.php?content=regist&topic=usermac&lang=th")
    temp = 0;
    for i in response:
        temp += i.count('./images/button_select.png')
    return temp

def changeOSToValue(os):
    value = '999'
    if 'Windows RT' == os:
         value='1008' 
    elif 'Windows 8' == os:
         value='1007' 
    elif 'Windows 7' == os:
         value='1000'
    elif 'Windows Vista' == os:
         value='21'
    elif 'Windows 2008' == os:
         value='1002'
    elif 'Windows XP' == os:
         value='1'
    elif 'Windows 2003' == os:
         value='3'
    elif 'Windows 2000' == os:
         value='2'
    elif 'Windows NT' == os:
         value='4' 
    elif 'Windows ME' == os:
         value='5'
    elif 'Windows 98' == os:
         value='6'
    elif 'Windows 95' == os:
         value='7'
    elif 'Windows 3.1' == os:
         value='8'
    elif 'Windows CE' == os:
          value='9'
    elif 'Pocket PC' == os:
         value='10'
    elif 'Android' == os:
         value='1005'
    elif 'Bada' == os:
         value='1006'
    elif 'Linux' == os:
         value='11'
    elif 'iPhone OS' == os:
         value='1003'
    elif 'Palm OS' == os:
         value='12'
    elif 'Symbian OS' == os:
         value='13'
    elif 'Mac OS' == os:
         value='14'
    elif 'BSD Family' == os:
         value='15'
    elif 'Solaris' == os:
         value='16'
    elif 'AIX' == os:
         value='17'
    elif 'Unix (Other)' == os:
         value='18'
    elif 'IOS' == os:
         value='19'
    elif 'Blackberry OS' == os:
         value='1004'
    elif 'DOS' == os:
         value='20'
    elif 'Other' == os: 
         value='999' 
    return value

def changeTypeToValue(typeDevice):
    value = '9'
    if "Notebook" == typeDevice:
        value = '9'
    elif "PC" == typeDevice:
        value = '10'
    elif "Phone-Tablet" == typeDevice:
        value = '20' 
    return value

# data = ['']*7
# data[0] = "b5610545692"
# data[1] = "frank1093"
# data[2] = "Windows 7"

# data[3] = "7831C1BBFE06"
# data[4] = "PC"
# data[5] = "0879959565"
# data[6] = 'thanachot7@hotmail.com'
# print enroll()