from django import forms
from .models import Register

BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
device_choices = (('Notebook', 'Notebook'),
                            ('PC', 'PC'))
                            # ('Phone-Tablet', 'Phone-Tablet'))
os_choices = (('Windows RT', 'Windows RT'),
                            ('Windows 8', 'Windows 8'),
                            ('Windows 7', 'Windows 7'),
                            ('Windows Vista', 'Windows Vista'),
                            ('Windows 2008', 'Windows 2008'),
                            ('Windows XP', 'Windows XP'),
                            ('Windows 2003', 'Windows 2003'),
                            ('Windows 2000', 'Windows 2000'),
                            ('Windows NT', 'Windows NT'),
                            ('Windows ME', 'Windows ME'),
                            ('Windows 98', 'Windows 98'),
                            ('Windows 95', 'Windows 95'),
                            ('Windows CE', 'Windows CE'),
                            ('Pocket PC', 'Pocket PC'),
	                        ('Android', 'Android'),
                            ('Bada', 'Bada'),
                            ('Linux', 'Linux'),
                            ('iPhone OS', 'iPhone OS'),
	                        ('Palm OS', 'Palm OS'),
                            ('Symbian OS', 'Symbian OS'),
                            ('Mac OS', 'Mac OS'),
                            ('BSD Family', 'BSD Family'),
                            ('Solaris', 'Solaris'),
                            ('AIX', 'AIX'),
                            ('Unix (Other)', 'Unix (Other)'),
                            ('IOS', 'IOS'),
                            ('Blackberry OS', 'Blackberry OS'),
                            ('DOS', 'DOS'),
                            ('Other', 'Other'))


class RegisterForm(forms.ModelForm):
    class Meta:
        model = Register
        widgets = {	
            'identity_card': forms.TextInput(attrs={'class': 'form-control','style': 'width: 200px;'}),
            'MAC_address': forms.TextInput(attrs={'class': 'form-control','style': 'width: 200px;'}),
            'firstname': forms.TextInput(attrs={'class': 'form-control','style': 'width: 200px;'}),
            'lastname': forms.TextInput(attrs={'class': 'form-control','style': 'width: 200px;'}),
            'phone': forms.TextInput(attrs={'class': 'form-control	','style': 'width: 200px;'}),
            'email': forms.TextInput(attrs={'class': 'form-control','style': 'width: 200px;', 'type': 'email'}),
            'os': forms.Select(attrs={'class': 'form-control','style': 'width: 200px;'}, choices=os_choices),
            'typeDevice': forms.Select(attrs={'class': 'form-control','style': 'width: 200px;'}, choices=device_choices),
            'confirmation': forms.TextInput(attrs={'class': 'form-control','style': 'width: 200px;', 'readonly' : 'true', 'type': 'hidden'}),
            'confirmed_by': forms.TextInput(attrs={'class': 'form-control','style': 'width: 200px;', 'readonly' : 'true', 'type': 'hidden'}),

            # 'MAC_address': forms.TextInput(attrs={'disabled': 'True', 'value': MAC}),
            # 'description': forms.Textarea(
            #     attrs={'placeholder': 'Enter description here'}),
            }
