from django.shortcuts import render, render_to_response, RequestContext
from django import forms
from .forms import RegisterForm
from .models import Register
# from .admin import *
from mac import findMac
from django.http import HttpResponse, HttpResponseRedirect
import bot
from django.contrib.auth.models import User, Group, Permission

def home(request):
    return render_to_response("base.html",
    locals(),
    context_instance = RequestContext(request))


def contact(request):
    return render_to_response("contact.html",
    locals(),
    context_instance = RequestContext(request))


def register(request):
    form = RegisterForm(None)
    # print '---------------'
    mac = findMac(request)
    # if form.is_valid(): 
    #     check1 = Register.objects.filter(Identity_card=request.POST['Identity_card']).exists()
    #     check2 = Register.objects.filter(MAC_address=request.POST['MAC_address']).exists()
    #     if not check1 and not check2:
    #         save_it = form.save(commit = False)
    #         save_it.save()
    #         return render_to_response("regis.html",
    #             locals(),
    #             context_instance = RequestContext(request))
    #     else:
    #         return render_to_response("enroll_result.html",
    #             locals(),
    #             context_instance = RequestContext(request))
    return render_to_response("register2.html",
    locals(),
    context_instance = RequestContext(request))


def edit_favorite(request):
    if request.is_ajax():
        message = int(request.GET['num1']) + int(request.GET['num2'])
    else:
        message = "Not Ajax"
    return HttpResponse(message)

def register2(request):
    if request.is_ajax():
        # mac = findMac(request)
        if request.POST['status'] == 'load':
            form = RegisterForm(None)
        elif request.POST['status'] == 'check':
            form = RegisterForm(request.POST)
            firstname = request.POST['firstname']
            lastname = request.POST['firstname']
            email = request.POST['email']
            phone = request.POST['phone']
            identityCard = request.POST['identity_card']  
            MACaddress = request.POST['MAC_address']
            os = request.POST['os']
            typeDevice = request.POST['typeDevice']
            if form.is_valid(): 
                check1 = Register.objects.filter(identity_card=request.POST['identity_card']).exists()
                check2 = Register.objects.filter(MAC_address=request.POST['MAC_address']).exists()
                if not check1 and not check2:
                    save_it = form.save(commit = False)
                    save_it.save()
                    return render_to_response("enroll_result.html",
                        locals(),
                        context_instance = RequestContext(request))
                # else:
                #     return 
    return render_to_response("register2.html",
        locals(),
        context_instance = RequestContext(request))   

def contact2(request):
    if request.is_ajax():
        return render_to_response("contact2.html",
        locals(),
        context_instance = RequestContext(request))
    else:
        message = "Not Ajax"
    return HttpResponse(message)

def regis2(request):
    if request.is_ajax():
        return render_to_response("regis2.html",
        locals(),
        context_instance = RequestContext(request))
    else:
        message = "Not Ajax"
    return HttpResponse(message)

def checkUser(request):
    if request.is_ajax():
        form = RegisterForm(request.POST or None)
        mac = findMac(request)
        if form.is_valid(): 
            check1 = Register.objects.filter(Identity_card=request.POST['identity_card']).exists()
            check2 = Register.objects.filter(MAC_address=request.POST['MAC_address']).exists()
            if not check1 and not check2:
                save_it = form.save(commit = False)
                save_it.save()
                return render_to_response("regis.html",
                    locals(),
                    context_instance = RequestContext(request))
        else:
            return render_to_response("register2.html",
            locals(),
            context_instance = RequestContext(request))  
    else:
        message = "Not Ajax"

def endRegister(request):
    if request.is_ajax():
        return render_to_response("enroll_result.html",
                    locals(),
                    context_instance = RequestContext(request))

def moreInformation(request):
    if request.is_ajax():
        return render_to_response("more-information.html",
                    locals(),
                    context_instance = RequestContext(request))

def head(request):
    return render_to_response("head.html",
                    locals(),
                    context_instance = RequestContext(request))

def getMacAddress(request):
    return HttpResponse(findMac(request));

def activate(request):
    if not request.user.is_authenticated():
        return HttpResponse('Please login.');
    if not request.user.groups.filter(name='barcamp5').exists():
        return HttpResponse('An access is denied')
    try:
        id_card = request.session['id-card']
        del request.session['id-card']
    except KeyError:
        pass
    return render_to_response("activate.html",
                    locals(),
                    context_instance = RequestContext(request))

def showUser(request):
    if not request.user.is_authenticated():
        return HttpResponse('Please login.');
    if not request.user.groups.filter(name='barcamp5').exists():
        return HttpResponse('An access is denied')
    if Register.objects.filter(identity_card=request.POST['identity_card']).exists():
        data = Register.objects.get(identity_card=request.POST['identity_card'])
        fullname = data.firstname + ' ' + data.lastname
        confirmation = data.confirmation
    else:
        error = True
    return render_to_response("showUser.html",
                    locals(),
                    context_instance = RequestContext(request))

def enroll(request):
    if not request.user.is_authenticated():
        return HttpResponse('Please login.');
    if not request.user.groups.filter(name='barcamp5').exists():
        return HttpResponse('An access is denied')

    temp = Register.objects.get(identity_card=request.POST['identity_card'])
    data = ['']*7
    data[0] = request.POST['id']
    data[1] = request.POST['password']
    data[2] = temp.os
    e = temp.MAC_address.split(":")
    r = ''
    for i in e:
        r += i
    data[3] = r
    data[4] = temp.typeDevice
    data[5] = temp.phone
    data[6] = temp.email
    result = bot.enroll(data)
    check = 'false'
    if (result == 'Success'):
        temp.confirmation = True
        temp.confirmed_by = request.user.get_username()
        check = 'true'
        temp.save()
    return render_to_response("showResult.html",
                    locals(),
                    context_instance = RequestContext(request))

def countDevices(request):
    if not request.user.is_authenticated():
        return HttpResponse('Please login.');
    if not request.user.groups.filter(name='barcamp5').exists():
        return HttpResponse('An access is denied')
        
    data = ['']*2
    data[0] = request.POST['id']
    data[1] = request.POST['password']
    count = bot.countDevice(data)
    return render_to_response("showCount.html",
                    locals(),
                    context_instance = RequestContext(request))

def activate2(request):
    if not request.user.is_authenticated():
        return HttpResponse('Please login.');
    if not request.user.groups.filter(name='barcamp5').exists():
        return HttpResponse('An access is denied')
    # form = RegisterForm(request.POST or None, initial={'confirmed_by': request.user.get_username()})
    # form.confirmed_by = request.user.get_username()
    form = RegisterForm(request.POST or None)
    if form.is_valid(): 
            check1 = Register.objects.filter(identity_card=request.POST['identity_card']).exists()
            check2 = Register.objects.filter(MAC_address=request.POST['MAC_address']).exists()
            if not check1 and not check2:
                request.session['id-card'] = request.POST['identity_card']
                save_it = form.save(commit = False)
                save_it.save()
                return HttpResponseRedirect('')
    return render_to_response("enroll-form.html",
                    locals(),
                    context_instance = RequestContext(request))
