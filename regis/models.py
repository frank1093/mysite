from django.db import models
from django.core.validators import RegexValidator
from django.utils.encoding import smart_unicode
# Create your models here.
# class reForm(forms.ModelForm):
#     IDForm = forms.CharField(max_length=13, min_length=13)



class Register(models.Model):
    number = RegexValidator(r'^[0-9]*$', 'Only number are allowed.')
    id_length = RegexValidator(regex='^.{13}$', message='Length must be 13', code='nomatch')
    alphanumeric = RegexValidator(
        r'^.{17}[0-9A-Z:]*$',
        'Only XX:XX:XX:XX:XX:XX are allowed.')
    char_only = RegexValidator(
        r'^[a-zA-Z ]*$',
        'Only a-z.')
    identity_card = models.CharField(
        max_length=13 ,blank=False,
        null=False, validators=[number, id_length])
    MAC_address = models.CharField(
        max_length=17, blank=False,
        null=False, validators=[alphanumeric])
    firstname = models.CharField(
        max_length=30 ,blank=False,
        null=False, validators=[char_only])
    lastname = models.CharField(
        max_length=30 ,blank=False,
        null=False, validators=[char_only])
    phone = models.CharField(
        max_length=10 ,blank=False,
        null=False, validators=[number])
    email = models.EmailField(blank=False,
        null=False)
    os = models.CharField(max_length=20, blank=False,
        null=False)
    # os =forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)
    typeDevice = models.CharField(max_length=20, blank=False,
        null=False)
    confirmation = models.BooleanField(default=False) 
    confirmed_by = models.CharField(
        max_length=30 ,blank=True,
        null=True)

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    modified = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return "{0} {1}".format(self.firstname,self.lastname)





