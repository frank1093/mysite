import subprocess

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def findMac(request):
    p = subprocess.Popen(["arp", get_client_ip(request)], stdout=subprocess.PIPE)
    out, err = p.communicate()
    out = out.splitlines()
    if out.__len__() == 1:
        out = out[0].split()
        if out.count('--') == 0:
            mac = out[3].split(':')
            result = ''
            for i in mac:
                result += '{:0>2}'.format(i.upper())
                result += ':'
            return result[:-1]
    return 'unknow'
