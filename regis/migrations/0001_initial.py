# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Register',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Identity_card', models.CharField(max_length=13, validators=[django.core.validators.RegexValidator(b'^[0-9]*$', b'Only number are allowed.'), django.core.validators.RegexValidator(regex=b'^.{13}$', message=b'Length must be 13', code=b'nomatch')])),
                ('MAC_address', models.CharField(max_length=17, validators=[django.core.validators.RegexValidator(b'^.{17}[0-9A-Z:]*$', b'Only XX:XX:XX:XX:XX:XX are allowed.')])),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
