# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0002_auto_20141026_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='register',
            name='confirmation',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='register',
            name='email',
            field=models.EmailField(default='test@re,com', max_length=75),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='register',
            name='os',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='register',
            name='typeDevice',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='register',
            name='firstname',
            field=models.CharField(max_length=30, validators=[django.core.validators.RegexValidator(b'^[a-zA-Z ]*$', b'Only a-z.')]),
        ),
        migrations.AlterField(
            model_name='register',
            name='lastname',
            field=models.CharField(max_length=30, validators=[django.core.validators.RegexValidator(b'^[a-zA-Z ]*$', b'Only a-z.')]),
        ),
    ]
