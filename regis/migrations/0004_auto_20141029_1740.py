# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0003_auto_20141029_1638'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register',
            name='os',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='register',
            name='typeDevice',
            field=models.CharField(max_length=20),
        ),
    ]
