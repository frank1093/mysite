# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='register',
            old_name='Identity_card',
            new_name='identity_card',
        ),
        migrations.AddField(
            model_name='register',
            name='firstname',
            field=models.CharField(default=0, max_length=30, validators=[django.core.validators.RegexValidator(b'^[a-zA-Z]*$', b'Only a-z.')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='register',
            name='lastname',
            field=models.CharField(default=0, max_length=30, validators=[django.core.validators.RegexValidator(b'^[a-zA-Z]*$', b'Only a-z.')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='register',
            name='phone',
            field=models.CharField(default=0, max_length=10, validators=[django.core.validators.RegexValidator(b'^[0-9]*$', b'Only number are allowed.')]),
            preserve_default=False,
        ),
    ]
