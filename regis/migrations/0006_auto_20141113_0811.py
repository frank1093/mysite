# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0005_register_confirmed_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register',
            name='confirmed_by',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
    ]
