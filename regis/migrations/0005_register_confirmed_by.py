# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('regis', '0004_auto_20141029_1740'),
    ]

    operations = [
        migrations.AddField(
            model_name='register',
            name='confirmed_by',
            field=models.CharField(max_length=30, null=True),
            preserve_default=True,
        ),
    ]
