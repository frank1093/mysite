from django.conf.urls import patterns, include, url
from django.contrib import admin
import regis

urlpatterns = patterns('',
    # Examples:
    # url(r'^test/', 'mysite.views.home', name='home2'),
    url(r'^$', 'regis.views.home', name='home'),
    url(r'^regis2/', 'regis.views.regis2', name='regis2'),
    # url(r'^contact/', 'regis.views.contact', name='contact'),
    url(r'^contact2/', 'regis.views.contact2', name='contact2'),
    # url(r'^register/', 'regis.views.register', name='register'),
	url(r'^register2/', 'regis.views.register2', name='register2'),
    url(r'^end-register/', 'regis.views.endRegister', name='endRegister'),
    url(r'^more-information/', 'regis.views.moreInformation', name='moreInformation'),
    url(r'^findmac/', 'regis.views.getMacAddress', name='get_MAC_address'),
    url(r'^activate/', 'regis.views.activate', name='activate'),
    url(r'^showUser/', 'regis.views.showUser', name='showUser'),
    url(r'^showCount/', 'regis.views.countDevices', name='showCount'),
    url(r'^enroll/', 'regis.views.enroll', name='enroll'),


    url(r'^activate2/', 'regis.views.activate2', name='activate2'),

    # url(r'^edit_favorite/', 'regis.views.edit_favorite', name='edit_favorite'),
    url(r'^head/', 'regis.views.head', name='head'),

    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # url(r'^regis/', 'regis.views.home', name
)
